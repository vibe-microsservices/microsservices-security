#/bin/bash
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

# Deploy auth-sso-common library on nexus
export NEXUS_NAMESPACE=cicd-devtools
export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/

#necessário compilar com java 8
export JAVA_HOME=//Library/Java/JavaVirtualMachines/jdk1.8.0_291.jdk/Contents/Home/
export PATH=$JAVA_HOME/bin:$PATH

mvn clean package deploy -DnexusReleaseRepoUrl=$MAVEN_URL_RELEASES -DnexusSnapshotRepoUrl=$MAVEN_URL_SNAPSHOTS -s ./maven-settings.xml -e -X -pl auth-sso-common 