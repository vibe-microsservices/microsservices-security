#/bin/bash

# Login no Openshift
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

#rm -rf microservices-security

export PROJECT_NAMESPACE=microservices-security
oc new-project $PROJECT_NAMESPACE
#git clone https://github.com/aelkz/microservices-security.git
#cd microservices-security/

# download maven settings.xml file
curl -o maven-settings-template.xml -s https://raw.githubusercontent.com/aelkz/microservices-security/master/_configuration/nexus/maven-settings-template.xml
# change mirror url using your nexus openshift route
export NEXUS_NAMESPACE=cicd-devtools
export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/
echo $MAVEN_URL
echo $MAVEN_URL_RELEASES
echo $MAVEN_URL_SNAPSHOTS
awk -v path="$MAVEN_URL" '/<url>/{sub(/>.*</,">"path"<")}1' maven-settings-template.xml > maven-settings.xml
rm -fr maven-settings-template.xml



#DEPLOY APPS

#PARENT
# Deploy parent project on nexus
mvn clean package deploy -DnexusReleaseRepoUrl=$MAVEN_URL_RELEASES -DnexusSnapshotRepoUrl=$MAVEN_URL_SNAPSHOTS -s ./maven-settings.xml -e -X -N
#stock
#oc new-app registry.access.redhat.com/ubi8/openjdk-8:1.19-1~https://gitlab.com/vibe-microsservices/microsservices-security/ -l app=stock --name=stock-api --context-dir=/stock --build-env='MAVEN_MIRROR_URL='${MAVEN_URL} -e MAVEN_MIRROR_URL=${MAVEN_URL}

