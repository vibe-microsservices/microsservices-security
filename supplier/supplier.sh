#/bin/bash
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

export NEXUS_NAMESPACE=cicd-devtools
export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/

#necessário compilar com java 8
export JAVA_HOME=//Library/Java/JavaVirtualMachines/jdk1.8.0_291.jdk/Contents/Home/
export PATH=$JAVA_HOME/bin:$PATH

export PROJECT_NAMESPACE=microservices-security

oc project $PROJECT_NAMESPACE

oc new-app registry.access.redhat.com/ubi8/openjdk-8:1.19-1~https://gitlab.com/vibe-microsservices/microsservices-security/ -l app=supplier-api --name=supplier-api --context-dir=/supplier --build-env='MAVEN_MIRROR_URL='${MAVEN_URL} -e MAVEN_MIRROR_URL=${MAVEN_URL} --as-deployment-config

oc patch svc supplier-api -p '{"spec":{"ports":[{"name":"http","port":8080,"protocol":"TCP","targetPort":8080}]}}'

oc label svc supplier-api monitor=springboot2-api

oc create -f configuration/configmap/supplier-api-env.yml -n ${PROJECT_NAMESPACE}
oc create -f configuration/secret/supplier-api.yml -n ${PROJECT_NAMESPACE}

export APP=supplier-api

oc set env dc/${APP} --from=secret/supplier-api-secret
oc set env dc/${APP} --from=configmap/supplier-api-config