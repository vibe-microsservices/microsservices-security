#/bin/bash
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

export NEXUS_NAMESPACE=cicd-devtools

export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/

#necessário compilar com java 8
export JAVA_HOME=//Library/Java/JavaVirtualMachines/jdk1.8.0_291.jdk/Contents/Home/
export PATH=$JAVA_HOME/bin:$PATH

export PROJECT_NAMESPACE=microservices-security

oc import-image rhscl/nodejs-10-rhel7 --from=registry.redhat.io/rhscl/nodejs-10-rhel7 -n openshift --confirm


export APIS_NAMESPACE=microservices-security
export THREESCALE_NAMESPACE=3scale
export RHSSO_NAMESPACE=rhsso
export RHSSO_URL=https://$(oc get route -n ${RHSSO_NAMESPACE} | grep secured | awk '{print $2;}')/auth
export THREESCALE_APP_DOMAIN=apps.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com
export THREESCALE_API_URL=https://$(oc get routes -n ${THREESCALE_NAMESPACE} | grep auth-integration | grep production | awk '{print $2;}')
export INTEGRATION_HEALTH_URL=http://$(oc get routes -n ${APIS_NAMESPACE} | grep auth-integration | grep metrics | awk '{print $2;}')


echo -e \
" AUTH_CLIENT_ID=41583d4f\n" \
"AUTH_URL=${RHSSO_URL}\n" \
"AUTH_REALM=3scale-api\n" \
"KEYCLOAK=true\n" \
"INTEGRATION_URI=${THREESCALE_API_URL}\n" \
"PRODUCT_PATH=/product\n" \
"STOCK_PATH=/stock\n" \
"SUPPLIER_PATH=/supplier\n" \
"AUTH_CLIENT_SECRET=5f92f6ae27c12976781913e7634cba0e\n" \
"NODE_TLS_REJECT_UNAUTHORIZED=0\n" \
> temp

sed "s/^.//g" temp >> nodejs-config.properties

rm -fr temp

oc create configmap nodejs-web-config \
--from-literal=AUTH_CLIENT_ID= \
--from-literal=AUTH_URL= \
--from-literal=AUTH_REALM= \
--from-literal=KEYCLOAK= \
--from-literal=INTEGRATION_URI= \
--from-literal=PRODUCT_PATH= \
--from-literal=SUPPLIER_PATH= \
--from-literal=STOCK_PATH= \
--from-literal=AUTH_CLIENT_SECRET= \
--from-literal=NODE_TLS_REJECT_UNAUTHORIZED=

oc new-app nodejs-10-rhel7:latest~https://github.com/aelkz/microservices-security.git --name=nodejs-web --context-dir=/webapp -l app=webapp --as-deployment-config -n ${APIS_NAMESPACE} 

# with the properties defined, set the environment variable on nodejs-web container.
oc set env --from=configmap/nodejs-web-config dc/nodejs-web -n ${APIS_NAMESPACE}

oc create route edge --service=nodejs-web --cert=server.cert --key=server.key -n ${APIS_NAMESPACE}




