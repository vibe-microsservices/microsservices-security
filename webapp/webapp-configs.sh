#/bin/bash
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

export NEXUS_NAMESPACE=cicd-devtools

export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/

#necessário compilar com java 8
export JAVA_HOME=//Library/Java/JavaVirtualMachines/jdk1.8.0_291.jdk/Contents/Home/
export PATH=$JAVA_HOME/bin:$PATH

export PROJECT_NAMESPACE=microservices-security

export APIS_NAMESPACE=microservices-security
export THREESCALE_NAMESPACE=3scale
export RHSSO_NAMESPACE=rhsso
export RHSSO_HOST=$(oc get route sso  -n $NAMESPACE --template='{{.spec.host}}')
export RHSSO_URL=https://$RHSSO_HOST/auth
export THREESCALE_APP_DOMAIN=apps.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com
export THREESCALE_API_URL=https://$(oc get routes -n ${THREESCALE_NAMESPACE} | grep auth-integration | grep production | awk '{print $2;}')
export INTEGRATION_HEALTH_URL=http://$(oc get routes -n ${APIS_NAMESPACE} | grep auth-integration | grep metrics | awk '{print $2;}')

oc project $PROJECT_NAMESPACE


# AUTH_CLIENT_ID=41583d4f - AUTH-INTEGRATION
# AUTH_CLIENT_SECRET=5f92f6ae27c12976781913e7634cba0e - AUTH-INTEGRATION

oc create configmap nodejs-web-config \
--from-literal=AUTH_CLIENT_ID=41583d4f \
--from-literal=AUTH_URL=${RHSSO_URL} \
--from-literal=AUTH_REALM=3scale-api \
--from-literal=KEYCLOAK=true \
--from-literal=INTEGRATION_URI=${THREESCALE_API_URL} \
--from-literal=PRODUCT_PATH=/product \
--from-literal=SUPPLIER_PATH=/supplier \
--from-literal=STOCK_PATH=/stock \
--from-literal=AUTH_CLIENT_SECRET=5f92f6ae27c12976781913e7634cba0e \
--from-literal=NODE_TLS_REJECT_UNAUTHORIZ`ED=0

# with the properties defined, set the environment variable on nodejs-web container.
oc set env --from=configmap/nodejs-web-config dc/nodejs-web -n ${APIS_NAMESPACE}

oc create route edge --service=nodejs-web --cert=server.cert --key=server.key -n ${APIS_NAMESPACE}




