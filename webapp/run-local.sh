#!/usr/bin/env bash

export PORT=4200
export AUTH_URL="https://sso-rhsso.apps.cluster-n5g2g.n5g2g.sandbox1575.opentlc.com/auth"
export AUTH_REALM="3scale-api"
export AUTH_CLIENT_ID="41583d4f"
export KEYCLOAK="true"

export INTEGRATION_URI="https://auth-integration-api-3scale-apicast-production.apps.cluster-n5g2g.n5g2g.sandbox1575.opentlc.com"
export PRODUCT_PATH="/product"
export STOCK_PATH="/stock"
export SUPPLIER_PATH="/supplier"

npm run start
