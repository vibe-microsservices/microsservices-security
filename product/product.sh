#/bin/bash
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

# Deploy auth-sso-common library on nexus
export NEXUS_NAMESPACE=cicd-devtools
export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export MAVEN_URL_RELEASES=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-releases/
export MAVEN_URL_SNAPSHOTS=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-snapshots/

#necessário compilar com java 8
export JAVA_HOME=//Library/Java/JavaVirtualMachines/jdk1.8.0_291.jdk/Contents/Home/
export PATH=$JAVA_HOME/bin:$PATH

export PROJECT_NAMESPACE=microservices-security

oc project $PROJECT_NAMESPACE

oc new-app registry.access.redhat.com/ubi8/openjdk-8:1.19-1~https://gitlab.com/vibe-microsservices/microsservices-security/ -l app=product-api --name=product-api --context-dir=/product --build-env='MAVEN_MIRROR_URL='${MAVEN_URL} -e MAVEN_MIRROR_URL=${MAVEN_URL} --as-deployment-config

oc patch svc product-api -p '{"spec":{"ports":[{"name":"http","port":8080,"protocol":"TCP","targetPort":8080}]}}'

oc label svc product-api monitor=springboot2-api

oc apply -f configuration/configmap/product-api-env.yml -n ${PROJECT_NAMESPACE}
oc apply -f configuration/secret/product-api.yml -n ${PROJECT_NAMESPACE}

export APP=product-api

oc set env dc/${APP} --from=secret/product-api-secret
oc set env dc/${APP} --from=configmap/product-api-config