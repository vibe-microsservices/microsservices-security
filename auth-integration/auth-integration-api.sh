#/bin/bash

# Login no Openshift
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true


# import a new spring-boot camel template
curl -o s2i-microservices-fuse74-spring-boot-camel.yaml -s https://gitlab.com/vibe-microsservices/microsservices-security/-/raw/main/_configuration/openshift/s2i-microservices-fuse74-spring-boot-camel.yaml
curl -o s2i-microservices-fuse74-spring-boot-camel-selfsigned.yaml -s https://gitlab.com/vibe-microsservices/microsservices-security/-/raw/main/_configuration/openshift/s2i-microservices-fuse74-spring-boot-camel-selfsigned.yaml


#oc delete template s2i-microservices-fuse74-spring-boot-camel -n ${PROJECT_NAMESPACE}

# NOTE. You may want to check the ..self-signed.yaml template as it uses a customized imagestream for use with self-signed certificates. (see the APPENDIX-README.md for for info)
export NEXUS_NAMESPACE=cicd-devtools
export PROJECT_NAMESPACE=microservices-security
export APP=auth-integration-api
export APP_NAME=auth-integration
export APP_GROUP=com.redhat.microservices
export APP_GIT=https://gitlab.com/vibe-microsservices/microsservices-security.git
export APP_GIT_BRANCH=main
export MAVEN_URL=http://$(oc get route nexus3 -n ${NEXUS_NAMESPACE} --template='{{ .spec.host }}')/repository/maven-group/
export CUSTOM_TEMPLATE=s2i-microservices-fuse74-spring-boot-camel-selfsigned

export TEMPLATE=s2i-fuse710-spring-boot-2-camel

oc project $PROJECT_NAMESPACE

oc create -n ${PROJECT_NAMESPACE} -f s2i-microservices-fuse74-spring-boot-camel.yaml
oc create -n ${PROJECT_NAMESPACE} -f s2i-microservices-fuse74-spring-boot-camel-selfsigned.yaml
oc create -n ${PROJECT_NAMESPACE} -f s2i-fuse710-spring-boot-2-camel.yaml
# the previous template have some modifications regarding services,route and group definitions.
# oc delete all -lapp=${APP}
oc new-app --template=${TEMPLATE} --name=${APP} --build-env='MAVEN_MIRROR_URL='${MAVEN_URL} -e MAVEN_MIRROR_URL=${MAVEN_URL} --param GIT_REPO=${APP_GIT} --param APP_NAME=${APP} --param ARTIFACT_DIR=${APP_NAME}/target --param GIT_REF=${APP_GIT_BRANCH} --param MAVEN_ARGS_APPEND='-pl '${APP_NAME}' --also-make' -l app=auth-integration-api --as-deployment-config

# Use this param if using a different TAG (example)
# --param BUILDER_VERSION=2.0

# check the created services:
# 1 for default app-context and 1 for /metrics endpoint.
oc get svc -n ${PROJECT_NAMESPACE} | grep ${APP_NAME}

# in order to auth-integration-api call the others APIs, we need to change it's configuration:
curl -o application.yaml -s https://gitlab.com/vibe-microsservices/microsservices-security/-/raw/main/auth-integration/src/main/resources/application.yaml

# NOTE. If you have changed the service or application's name, you need to edit and change the downloaded application.yaml file with your definitions.

# create a configmap and mount a volume for auth-integration-api
#oc delete configmap ${APP} -n ${PROJECT_NAMESPACE}

oc create -f configuration/configmap/auth-integration-api-env.yml -n ${PROJECT_NAMESPACE}
oc create -f configuration/secret/auth-integration-api.yml -n ${PROJECT_NAMESPACE}
oc create -f configuration/service/ -n ${PROJECT_NAMESPACE}


oc set env dc/${APP} --from=secret/auth-integration-api-secret
oc set env dc/${APP} --from=configmap/auth-integration-api-config