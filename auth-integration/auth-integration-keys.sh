#/bin/bash

# Login no Openshift
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true

export RHSSO_NAMESPACE=rhsso
export RHSSO_REALM=3scale-api
export RHSSO_URI=$(oc get route sso  -n $RHSSO_NAMESPACE --template='{{.spec.host}}')
export TOKEN_URL=https://${RHSSO_URI}/auth/realms/${RHSSO_REALM}/protocol/openid-connect/token
export THREESCALE_REALM_USERNAME=admin
export THREESCALE_REALM_PASSWORD=rhsso



TKN=$(curl -k -X POST "$TOKEN_URL" \
 -H "Content-Type: application/x-www-form-urlencoded" \
 -d "username=$THREESCALE_REALM_USERNAME" \
 -d "password=$THREESCALE_REALM_PASSWORD" \
 -d "grant_type=password" \
 -d "client_id=admin-cli")

echo $TKN

# Retrieve realm keys
KEYS=$(curl -H "Authorization: Bearer $TKN" "https://${RHSSO_URI}/auth/realms/${RHSSO_REALM}")
echo $KEYS
RSA_PUB_KEY=$(echo "$KEYS" | jq -r '.public_key')

echo "Public Key:"
echo "$RSA_PUB_KEY"

REALM_CERT=$RHSSO_REALM.pem

echo "-----BEGIN CERTIFICATE-----" > $REALM_CERT; echo $RSA_PUB_KEY >> $REALM_CERT; echo "-----END CERTIFICATE-----" >> $REALM_CERT

fold -s -w 64 $REALM_CERT > $RHSSO_REALM.fixed.pem
openssl x509 -in $RHSSO_REALM.fixed.pem -text -noout
openssl x509 -in $RHSSO_REALM.fixed.pem -noout -issuer -fingerprint
