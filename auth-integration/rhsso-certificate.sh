#!/bin/bash

# Keycloak administration credentials
KEYCLOAK_URL="https://sso-rhsso.apps.cluster-n5g2g.n5g2g.sandbox1575.opentlc.com/auth"
ADMIN_USERNAME="admin"
ADMIN_PASSWORD="rhsso"
REALM_NAME="3scale-api"

# Authenticate and obtain access token
access_token=$(curl -X POST \
  -d "client_id=admin-cli" \
  -d "username=$ADMIN_USERNAME" \
  -d "password=$ADMIN_PASSWORD" \
  -d "grant_type=password" \
  "$KEYCLOAK_URL/realms/master/protocol/openid-connect/token" | jq -r '.access_token')

# Retrieve public keys for the realm
public_keys=$(curl -X GET \
  -H "Authorization: Bearer $access_token" \
  "$KEYCLOAK_URL/admin/realms/$REALM_NAME/keys")


# Extract and display public key certificates
echo "public key: "
echo "$public_keys" | jq -r '.keys[1].publicKey'
echo "certificate: "
echo "$public_keys" | jq -r '.keys[1].certificate'