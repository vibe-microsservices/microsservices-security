#/bin/bash


# Login no Openshift
export USER=admin
export GUIDE="n5g2g"
export SANDBOXID=1575
export PASSWORD=7oxw4s35OomUD3dm
export API_SERVER=https://api.cluster-$GUIDE.$GUIDE.sandbox$SANDBOXID.opentlc.com:6443

oc login -u $USER -p $PASSWORD $API_SERVER --insecure-skip-tls-verify=true
export NAMESPACE=rhsso

oc project $NAMESPACE

export RHSSO_REALM=3scale-api
export RHSSO_URI=$(oc get route sso  -n $NAMESPACE --template='{{.spec.host}}')
export TOKEN_URL=https://${RHSSO_URI}/auth/realms/${RHSSO_REALM}/protocol/openid-connect/token
export THREESCALE_REALM_USERNAME=admin
export THREESCALE_REALM_PASSWORD=12345

echo $RHSSO_URI
echo $TOKEN_URL

TKN=$(curl -k -X POST "$TOKEN_URL" \
-H "Content-Type: application/x-www-form-urlencoded" \
-d "username=$THREESCALE_REALM_USERNAME" \
-d "password=$THREESCALE_REALM_PASSWORD" \
-d "grant_type=password" \
-d "client_id=admin-cli" \
| sed 's/.*access_token":"//g' | sed 's/".*//g')

echo $TNK

export REALM_KEYS_URL=https://${RHSSO_URI}/auth/admin/realms/${RHSSO_REALM}/keys

RSA_PUB_KEY=$(curl -k -X GET "$REALM_KEYS_URL" \
-H "Authorization: Bearer $TKN" \
| jq -r '.keys[] | select(.type=="RSA") | .publicKey' )

echo $RSA_PUB_KEY

# Create a valid .pem certificate
REALM_CERT=$RHSSO_REALM.pem
echo "-----BEGIN CERTIFICATE-----" > $REALM_CERT; echo $RSA_PUB_KEY >> $REALM_CERT; echo "-----END CERTIFICATE-----" >> $REALM_CERT

# Check the generated .pem certificate
 fold -s -w 64 $REALM_CERT > $RHSSO_REALM.fixed.pem
 openssl x509 -in $RHSSO_REALM.fixed.pem -text -noout
 openssl x509 -in $RHSSO_REALM.fixed.pem -noout -issuer -fingerprint

