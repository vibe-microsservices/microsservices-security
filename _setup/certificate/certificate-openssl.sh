#/bin/bash

export RHSSO_REALM=3scale-api
echo | openssl s_client -servername sso-rhsso.apps.cluster-n5g2g.n5g2g.sandbox1575.opentlc.com -connect sso-rhsso.apps.cluster-n5g2g.n5g2g.sandbox1575.opentlc.com:443 2>/dev/null | openssl x509 > $RHSSO_REALM.pem

openssl x509 -pubkey -noout -in $RHSSO_REALM.pem > public_key.pem
